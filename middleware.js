"use strict";

const TextToSpeechV1 = require('ibm-watson/text-to-speech/v1');
const { IamAuthenticator } = require('ibm-watson/auth');

module.exports = function(req, res, next) {
  const textToSpeech = new TextToSpeechV1({
    authenticator: new IamAuthenticator({ 
      apikey: process.env.APIKEY
    }),
    url: process.env.URL
  });
  req.textToSpeech = textToSpeech;
  next();
};