const express = require('express');
const uuidv5 = require('uuid/v5');
const fs = require('fs');

const router = express.Router();

router.post('/synthesize', function(req, res) {
  const textToSpeech = req.textToSpeech;
  
  const synthesizeParams = {
    text: req.body.text,
    accept: 'audio/wav',
    voice: 'en-US_AllisonVoice',
  };

  textToSpeech
  .synthesize(synthesizeParams)
  .then(response => {
    const audio = response.result;
    return textToSpeech.repairWavHeaderStream(audio);
  })
  .then(repairedFile => {
    const path = 'audios';
    const name = `${uuidv5.URL}.wav`;
    
    if (!fs.existsSync(path)){
      fs.mkdirSync(path);
    }

    fs.writeFileSync(`${path}/${name}`, repairedFile);
    return res.json({
      audio: name
    });
  })
  .catch(err => {
    return res.json({
      message: err
    });
  });
});

module.exports = router;